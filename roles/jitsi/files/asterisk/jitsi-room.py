#!/usr/bin/python3
# Managed by ansible.

import re
import configparser
from argparse import ArgumentParser
from random import choice
from string import ascii_letters
from urllib.parse import urlencode

import requests

p = ArgumentParser()
p.add_argument('room_number', type=int, help='Room PIN provided by user')
p.add_argument('caller_id', help='Caller ID')
p.add_argument('-c', '--config', default='/etc/asterisk/jitsi_mapper.conf',
               help='Configuration file')
args = p.parse_args()

config = configparser.ConfigParser()
config.read(args.config)
GLOBAL_CONFIG = config['global']
ROOM_PASSWORDS = config['room_passwords']
KNOWN_CALLERS = config['known_callers']

r = requests.get(GLOBAL_CONFIG.get('conference_mapper')
                 + '?' + urlencode({'id': args.room_number}))
assert r.status_code == 200
jitsi_room = r.json()['conference']
room_name = ''
if jitsi_room:
    room_name, host_name = jitsi_room.rsplit('@', 1)
    assert '"' not in room_name
    assert host_name == 'conference.{}'.format(GLOBAL_CONFIG.get('host_name'))
print('SET VARIABLE JITSI_ROOM "{}"'.format(room_name))

room_password = ROOM_PASSWORDS.get(room_name, '')
print('SET VARIABLE JITSI_PW "{}"'.format(room_password))

m = re.match(r'^(.*) <([+0-9. -]*)>$', args.caller_id)
caller_id = ''.join(choice(ascii_letters) for i in range(4))
if m:
    if m.group(2) in KNOWN_CALLERS:
        caller_id = KNOWN_CALLERS[m.group(2)]
    elif m.group(1) and not m.group(1).isdigit():
        caller_id = m.group(1)
    elif m.group(2):
        caller_id = '...{}'.format(m.group(2)[-4:])
caller_id = "Phone: {}".format(caller_id)
print('SET VARIABLE JITSI_CALLER "{}"'.format(caller_id))

#with open('/tmp/debug', 'w') as f:
#    f.write('{}\n'.format(args.room_number))
#    f.write('{}\n'.format(args.caller_id))
#    f.write('{}\n'.format(room_name))
#    f.write('{}\n'.format(caller_id))
