# vim: ft=ruby
require 'yaml/store'

# First, set up a bunch of variables for the ansible run later.
streaming = {
  "method" => "rtmp",
  "rtmp" => {
    "vaapi" => false,
    "location" => "rtmp://streamer-backend.dev.local:1935/stream/main live=1",
  },
  "backend" => {
    "server_name" => "streamer-backend.dev.local",
    "rtmp_push_uris" => [ "rtmp://streamer-frontend.dev.local/front" ],
  },
  "rooms" => [ "main" ],
}

staticips = {
  "gateway" => "storage",
  "hosts" => [
    {"hostname" => "storage", "ip" => "10.10.10.21", "fqdn" => "storage.dev.local", "memory" => 512},
    {"hostname" => "vocto", "ip" => "10.10.10.22", "fqdn" => "vocto.dev.local", "memory" => 1024},
    {"hostname" => "streamer-backend", "ip" => "10.10.10.23", "fqdn" => "streamer-backend.dev.local", "memory" => 512},
    {"hostname" => "encoder1", "ip" => "10.10.10.24", "fqdn" => "encoder1.dev.local", "memory" => 512},
    {"hostname" => "streamer-frontend", "ip" => "10.10.10.25", "fqdn" => "streamer-frontend.dev.local", "memory" => 512},
    {"hostname" => "loopy", "ip" => "10.10.10.26", "fqdn" => "loopy.dev.local", "memory" => 512},
  ],
  "write_hosts" => true,
  "write_interfaces" => true,
}

# Write the variables to a YAML file, together with a few simple
# boolean/string variables
store = YAML::Store.new "vagrant-vars.yml"
store.transaction do
  store["staticips"] = staticips
  store["nfs_server"] = "storage.dev.local"
  store["in_vagrant"] = true
  store["gridengine_main_host"] = "storage.dev.local"
  store["self_sign"] = true
  store["streaming"] = streaming
  store["room_name"] = "main"
  store["irc_room_channel"] = "#debconf-video-test"
  store["irc_nick"] = "vagrant_testuser"
  store["autologin"] = true
  store["sreview"] = { "use_private_repo" => true }
  store["obs_public_ip"] = staticips["hosts"][5]["ip"]
end

# Now do the Vagrant configuration
Vagrant.configure("2") do |config|
  staticips["hosts"].each do |host|
    config.vm.define host["hostname"] do |config|
      config.vm.box = "debian/bookworm64"
      config.vm.hostname = host["fqdn"]
      config.vm.network "private_network", ip: host["ip"], hostname: true
      config.vm.provider :libvirt do |libvirt|
        libvirt.cpus = 1
        libvirt.memory = host["memory"]
      end
      if host["hostname"] == staticips["hosts"][-1]["hostname"]
        config.vm.provision :ansible, run: "always" do |ansible|
          ansible.limit = "all"
          ansible.playbook = "site.yml"
          ansible.groups = {
            "nfs-server" => ["storage"],
            "nfs-client" => ["encoder1"],
            "voctomix" => ["vocto"],
            "vogol" => ["vocto"],
            "streaming-backends" => ["streamer-backend"],
            "streaming-frontends" => ["streamer-frontend"],
            "ge-master" => ["storage"],
            "encoder" => ["storage","encoder1","encoder2"],
            "onsite:children" => ["ge-master","encoder","voctomix","streaming-backends"],
            "obs" => ["loopy"],
          }
          ansible.become = true
          ansible.host_vars = {
            "storage" => {
              "eth_local_address" => "10.10.10.21",
            },
          }
          ansible.raw_arguments = [
            "--extra-vars=@./vagrant-vars.yml"
          ]
          ansible.config_file = "tests/ansible.cfg"
        end
      end
    end
  end
  config.vm.provision :shell,
      inline: "adduser --disabled-password --gecos Videoteam videoteam"
end
