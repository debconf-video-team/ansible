This is what makes `url=video-setup.debian.net` work.

The preseed.cfg files are hosted at https://salsa.debian.org/debconf-video-team/ansible/raw/main/roles/pxe/tftp-server/files/d-i/bookworm/preseed.cfg, di's url magic will construct $hostname/d-i and this redirect ties the two together.

This means what is on salsa is what is used by the script, there is no extra synchronization step.
