---
# yamllint disable rule:line-length

image: registry.salsa.debian.org/debconf-video-team/ansible:ci

ci-image:
  stage: .pre
  image: docker:24-dind
  services:
    - name: docker:24-dind
  variables:
    DOCKER_DRIVER: overlay2
  script:
    - 'docker build -t $CI_REGISTRY_IMAGE:ci -f Dockerfile.ci .'
    - 'docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY'
    - 'docker push $CI_REGISTRY_IMAGE:ci'
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        paths:
          - Dockerfile.ci

stages:
  - lint
  - unittest
  - doc

.common: &common
  before_script:
    - apt-get update

.unittest: &unittest
  <<: *common
  stage: unittest
  script:
    - ansible-playbook tests/${CI_JOB_NAME}.yml -e '{"skip_unit_test":true}'
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - roles/$CI_JOB_NAME/**
          - tests/$CI_JOB_NAME.yml

ansible-lint:
  <<: *common
  stage: lint
  script:
    - apt-get -y install ansible-lint
    - PYTHONIOENCODING=UTF-8 ansible-lint site.yml
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

yamllint:
  <<: *common
  stage: lint
  script:
    - apt-get -y install yamllint
    - yamllint .
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

apt:
  <<: *unittest
  script:
    - apt-get update && apt-get -y install python3-apt
    - echo "deb cdrom:[foobar test]/ bookworm main" >> /etc/apt/sources.list
    - touch /etc/apt/apt.conf.d/30apt-proxy
    - rm /etc/apt/sources.list.d/*
    - ansible-playbook tests/${CI_JOB_NAME}.yml -e '{"skip_unit_test":true}'
    - apt-get update && apt-get -y install rolldice

bash:
  <<: *unittest
  script:
    - ansible-playbook tests/bash.yml
    - bash -c "compgen -c unam |grep -q uname && exit || exit 1"

etherpad:
  <<: *unittest

firewall:
  <<: *unittest

gnome-desktop:
  <<: *unittest

irc:
  <<: *unittest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - roles/$CI_JOB_NAME/**
          - tests/$CI_JOB_NAME.yml
          - roles/users/**
          - roles/xfce-desktop/**

jitsi:
  <<: *unittest

motd:
  <<: *unittest
  script:
    - ansible-playbook tests/motd.yml
    - sh tests/motd/unit_test.sh

nageru:
  <<: *unittest
  before_script:
    - sed -ie "s/main/main non-free/" /etc/apt/sources.list.d/*
    - apt-get update
  script:
    - adduser videoteam
    - ansible-playbook tests/nageru.yml -e '{ "skip_unit_test":true }'

nfs-client:
  <<: *unittest

nfs-server:
  <<: *unittest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - roles/$CI_JOB_NAME/**
          - tests/$CI_JOB_NAME.yml
          - roles/users/**

obs:
  <<: *unittest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - roles/$CI_JOB_NAME/**
          - tests/$CI_JOB_NAME.yml
          - roles/streaming/base/**

opsis:
  <<: *unittest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - roles/$CI_JOB_NAME/**
          - tests/$CI_JOB_NAME.yml
          - roles/opsis-ingest/**

prometheus-nginx-rtmp-exporter:
  <<: *unittest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - tests/$CI_JOB_NAME.yml
          - roles/prometheus/nginx-rtmp-exporter/**

prometheus-node-exporter:
  <<: *unittest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - tests/$CI_JOB_NAME.yml
          - roles/prometheus/node-exporter/**

prometheus-server:
  <<: *unittest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - tests/$CI_JOB_NAME.yml
          - roles/prometheus/server/**
          - roles/prometheus/grafana/**

sreview-master:
  <<: *unittest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - roles/sreview/setup/**
          - roles/sreview/master/**
          - roles/users/**
          - roles/nfs-server/**

sreview-previews-export:
  <<: *unittest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - roles/$CI_JOB_NAME/**
          - tests/$CI_JOB_NAME.yml
          - roles/sreview/previews-export/**

streaming-backend:
  <<: *unittest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - tests/$CI_JOB_NAME.yml
          - roles/streaming/base/**
          - roles/streaming/backend/**

streaming-frontend:
  <<: *unittest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - tests/$CI_JOB_NAME.yml
          - roles/streaming/base/**
          - roles/streaming/frontend/**

system-software:
  <<: *unittest

test-station:
  <<: *unittest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - roles/$CI_JOB_NAME/**
          - tests/$CI_JOB_NAME.yml
          - roles/users/**
          - roles/opsis/**

tftp-server:
  <<: *unittest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - tests/$CI_JOB_NAME.yml
          - roles/users/**
          - roles/pxe/tftp-server/**

tls-certificates:
  <<: *unittest

users:
  <<: *unittest

vim:
  <<: *unittest

vnc:
  <<: *unittest
  script:
    - adduser videoteam
    - ansible-playbook tests/vnc.yml -e '{ "skip_unit_test":true }'

voctomix:
  <<: *unittest
  script:
    - adduser videoteam
    - ansible-playbook tests/voctomix.yml -e '{ "skip_unit_test":true }'

vogol:
  <<: *unittest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - roles/$CI_JOB_NAME/**
          - tests/$CI_JOB_NAME.yml
          - roles/users/**

xfce-desktop:
  <<: *unittest

docs:
  <<: *common
  image: debian:bookworm
  stage: doc
  script:
    - apt-get -y install python3-sphinx-rtd-theme python3-sphinx python3-myst-parser make
    - cd docs
    - make html
    - mv _build/html/ ../public/
  artifacts:
    paths:
      - public
    expire_in: 1 day
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - docs/
          - "*.md"
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual
      allow_failure: true
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

pages:
  stage: doc
  script:
    - head -2 public/objects.inv
  needs:
    - job: docs
      artifacts: true
  artifacts:
    paths:
      - public
    expire_in: 1 hour
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
