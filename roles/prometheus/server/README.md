# prometheus/server

This role sets up a prometheus server ready to scrape data from other hosts in
its network. It also optionally configures Alertmanager to alert to IRC.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

The main variables for this role are:

* `prometheus_targets_directory`: Directory to import exported target
                                  definitions from.
* `prometheus_alertmanager_irc.irc_host`: The IRC network host for alerting
* `prometheus_alertmanager_irc.irc_port`: The IRC port for alerting
* `prometheus_alertmanager_irc.nick`: The IRC nick to use for alerting
* `prometheus_alertmanager_irc.webhook_host`: The hostname to listen for webhooks on
* `prometheus_alertmanager_irc.webhook_port`: The port to listen for webhooks on
* `prometheus_alertmanager_irc.channel`: The channel to alert to
* `prometheus_scrape_configs`: List of dictionaries of extra scrape configuration.
