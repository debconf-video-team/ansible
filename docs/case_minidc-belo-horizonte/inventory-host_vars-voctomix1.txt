irc_room_channel: "#minidc-bh-main"
irc_nick: videoteam_bh_main
room_name: main

#voctolights:
#  - camera: Cam1
#    plugin: serial_dtr
#    port: /dev/ttyUSB0

ingest_sources:
  - description: "webcam fifine"
    video_source: hdmi2usb
    video_attribs: device=/dev/video0
    audio_source: alsa
    port: 10000

streaming:
  rooms:
    - main
  method: rtmp
  rtmp:
    vaapi: false
    location: "rtmp://backend.live.debconf.org:1935/stream/{{ room_name }}"
  dash:
    video_codec: libsvtav1
    audio_codec: libopus
    preset: 8
    video_variants:
      - crf: 23
      - scale: 640x360
        crf: 23
      - scale: 320x180
        bitrate: 192k
        maxrate: 256k
    audio_variants:
      - bitrate: 128k
      - bitrate: 64k
  hq_config:
    hls_bandwidth: 2176000
    video_bitrate: 2000 # kbps
    audio_bitrate: 128000 # bps
    keyframe_period: 60 # seconds
    width: 1280
  adaptive:
    video_codec: libx264
    audio_codec: aac
    variants:
      high:
        hls_bandwidth: 1152000
        video_bitrate: 1024k
        audio_bitrate: 128k
        extra_settings: -tune zerolatency -preset veryfast -crf 23
        width: 900
      mid:
        hls_bandwidth: 448000
        video_bitrate: 768k
        audio_bitrate: 96k
        extra_settings: -tune zerolatency -preset veryfast -crf 23
        width: 720
      low:
        hls_bandwidth: 288000
        video_bitrate: 256k
        audio_bitrate: 48k
        extra_settings: -tune zerolatency -preset veryfast -crf 23
        width: 480
  dump: False
  mix_channels: false
