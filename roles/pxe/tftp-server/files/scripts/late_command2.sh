#!/bin/sh

# late_command2.sh - example and useful late_command.sh hook
# hooked by passing a kernel parameter to d-i:
# lc2_url=http://192.168.1.110:8007/scripts/late_command2.sh
# which can be done using: usbinst/configs/lc2.cfg

# not needed, just here for documentation and maybe debugging.
org_server=$1
# it is likely video-setup.debian.net
# if it is anything else, you probably don't need to be using lc2_url.

# prevent this script from being re-run when we re-run late_command.sh:
unset lc2_url

# rerun the normal late_command.sh, passing in a different server
# which is where it will wget late_command.cfg from
# thus allowing other plabooks and or inventories to be used.

# late_command.sh expects the IP:port of server hosting late_command.cfg
/tmp/late_command.sh 192.168.1.110:8007
