#!/bin/bash

set -e

# extract-desktopvideo-drivers.sh - setup the closed source black magic packages

usage () {
  echo ""
  echo "USAGE: "
  echo "$0 -f filename [-c cache_dir] [-U user -H destination_host] [-r destination_dir] [-w web_dir]"
  echo "    -c dir to untar into (default: ../pxe/tftp-server/files/bm)"
  echo "    -f file downloaded from vendor"
  echo "    -h show this help message"
  echo "    -H destination hostname (default: empty = don't upload)"
  echo "    -r destination dir, the http server's web root. (default /srv/www)"
  echo "    -U destination user (default videoteam)"
  echo "    -w dir under web root (default: bm)"
  echo ""
  echo "Instructions:
1. Download the tar.gz per the README
2. ./extract-desktopvideo-drivers.sh -f ~/Downloads/Blackmagic_Desktop_Video_Linux_12.5.tar.gz
3. cut/paste the yaml into your inventory yaml.

Cache dir defaults to ansible/roles/pxe/tftp-server/files/bm
because the d-i and scripts dirs are there,
which get hosted by ansible/usbinst/http_server.sh
So it might be handy to host the local bm files there."

  exit 1
}

if [ $# -eq 0 ]; then
  usage
fi

cache_dir=../pxe/tftp-server/files/bm
dest_user=videoteam
dest_host=""
dest_dir=/srv/www
web_dir=bm

# CLI arguments comprehension
while getopts "hf:c:U:H:r:w:" opt; do
  case $opt in
      h) usage;;
      f) bm_dl=$OPTARG;;
      c) cache_dir=$OPTARG;;
      U) dest_user=$OPTARG;;
      H) dest_host=$OPTARG;;
      r) dest_dir=$OPTARG;;
      w) web_dir=$OPTARG;;
      *) usage;;
  esac
done

# get the version from the downloaded filename:
bm_ver=$(echo ${bm_dl}|sed  -E "s/.*Blackmagic_Desktop_Video_Linux_(.*).tar.gz/\1/")
bm_dir=Blackmagic_Desktop_Video_Linux_${bm_ver}/deb/x86_64

mkdir -p ${cache_dir}
tar -xv --directory ${cache_dir} -f ${bm_dl}
cd ${cache_dir}

if [[ ${dest_host} ]]; then
  rsync -rtvP --mkpath ${bm_dir}/ ${dest_user}@${dest_host}:${dest_dir}/${web_dir}/${bm_dir}
else
  # using the local box as the host
  dest_host=$HOSTNAME
fi

dkms_version=$(ls ${bm_dir}/desktopvideo_* | sed  -E "s/.*desktopvideo_(.*)_amd64.deb/\1/")
mediaexpress_version=$(ls ${bm_dir}/mediaexpress* | sed  -E "s/.*mediaexpress_(.*)_amd64.deb/\1/")

cat << EOT
blackmagic:
  urls:
    desktopvideo: http://${dest_host}/${web_dir}/${bm_dir}/desktopvideo_${dkms_version}_amd64.deb
    desktopvideo_gui: http://${dest_host}/${web_dir}/${bm_dir}/desktopvideo-gui_${dkms_version}_amd64.deb
    mediaexpress: http://${dest_host}/${web_dir}/${bm_dir}/mediaexpress_${mediaexpress_version}_amd64.deb
  dkms_version: ${dkms_version}
EOT

