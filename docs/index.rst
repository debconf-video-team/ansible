Ansible Documentation
---------------------

.. toctree::
   :maxdepth: 2

   getting_started.rst
   general_layout.rst
   simple_setup.rst
   usb_install.rst
   advanced_usage.rst
   ansible_roles.rst
   contact.rst
   case_minidc-belo-horizonte.rst
