---
- name: Install Postgres for Grafana
  ansible.builtin.apt:
    name:
      - postgresql
      - python3-psycopg2

- name: Create grafana Postgres user
  community.postgresql.postgresql_user:
    name: grafana
  become: true
  become_user: postgres
  when: not skip_unit_test

- name: Create grafana Postgres DB
  community.postgresql.postgresql_db:
    name: grafana
    owner: grafana
  become: true
  become_user: postgres
  when: not skip_unit_test

- name: Add Grafana APT key
  ansible.builtin.copy:
    src: grafana.gpg
    dest: /usr/share/keyrings/grafana-repo.gpg

- name: Enable the Grafana APT repo
  ansible.builtin.apt_repository:
    repo: >
      deb [signed-by=/usr/share/keyrings/grafana-repo.gpg]
      https://packages.grafana.com/oss/deb stable main
    filename: grafana

- name: Pin Grafana packages
  ansible.builtin.copy:
    src: grafana-apt-pin
    dest: /etc/apt/preferences.d/grafana-apt-pin

- name: Install Grafana
  ansible.builtin.apt:
    name: grafana

- name: Enable Grafana Server
  ansible.builtin.systemd:
    name: grafana-server
    enabled: true
  when: not skip_unit_test

- name: Configure Grafana
  ansible.builtin.template:
    src: templates/grafana.ini.j2
    dest: /etc/grafana/grafana.ini
  notify: Restart grafana

- name: Force grafana to start before adding admin user
  ansible.builtin.meta: flush_handlers
- name: Create Grafana state directory
  ansible.builtin.file:
    dest: /etc/grafana/ansible-account-states
    mode: "0700"
    state: directory

- name: Update Grafana admin user
  ansible.builtin.copy:
    content: "{{ grafana_admin_password | string | hash('sha3_256') }}\n"
    dest: /etc/grafana/ansible-account-states/admin
  notify: Register grafana admin

- name: Import Grafana metrics into Promotheus
  ansible.builtin.copy:
    src: prometheus-exporter.yml
    dest: "{{ prometheus_targets_directory }}/grafana.yml"
