# prometheus/nginx-rtmp-exporter

This role sets up a prometheus nginx-rtmp exporter ready to be scraped
by a prometheus server.
It also sets up a scrape configuration file ready for the prometheus
server to pick up (See role `prometheus-server`).

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

The main variables for this role are:

* `prometheus_inventory_hostname`: Inventory hostname of the prometheus server
                                   that needs to scrape us.

* `prometheus_targets_directory`:  Directory on the prometheus server to store
                                   exported targets in.
